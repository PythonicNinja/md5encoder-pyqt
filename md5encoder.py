# -*- coding: utf-8 -*-
# CREATED ON DATE: 20.09.2014
__author__ = 'vojtek.nowak@gmail.com'


import sys
import hashlib

from PySide.QtCore import *
from PySide.QtGui import *

class WidgetEncoder(QWidget):
    def __init__(self, type):
        self.type = type
        super(WidgetEncoder, self).__init__()
        self.layout = QVBoxLayout()

        self.btn_layout = QHBoxLayout()
        self.btn_action = QPushButton(str(self.type))
        self.btn_layout.addWidget(self.btn_action)

        self.layout.addLayout(self.btn_layout)

        self.text_field = QTextEdit()
        self.layout.addWidget(self.text_field)

        self.setLayout(self.layout)

        self.connectSignals()

    def connectSignals(self):
        if self.type == 'encode':
            self.btn_action.clicked.connect(self.encode)
        elif self.type == 'decode':
            self.btn_action.clicked.connect(self.decode)

    def get_text_field_value(self):
        return self.text_field.toPlainText()

    def encode(self):
        text = self.get_text_field_value()
        text_encoded = hashlib.md5(text.encode('utf-8')).hexdigest()
        self.text_field.setText(text_encoded)

    def decode(self, *args, **kwargs):
        self.text_field.setText('This algorithm is not reversible')


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setWindowTitle("MD5 Encoder")

        self.apptab = QTabWidget()
        self.setCentralWidget(self.apptab)
        self.apptab.addTab(WidgetEncoder(type='encode'), "Encoder")
        self.apptab.addTab(WidgetEncoder(type='decode'), "Decoder")

        self.show()

def main():
    app = QApplication(sys.argv)
    main = MainWindow()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()